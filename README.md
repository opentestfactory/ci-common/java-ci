<!--

     Copyright (c) 2021 Henix, henix.fr

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.

-->

# java-ci

## Use this shared java pipeline in your project

### > Define your project's gitlab-ci

This repository contains the gitlab-ci pipeline shared between otf Java components.

Your project will use this shared configuration by importing `gitlab-ci-java.yml` in its own `.gitlab-ci.yml` :

``` yaml
include:
  - project: 'opentestfactory/ci-common/java-ci'
    ref: main
    file: 'gitlab-ci-java.yml'
```
### Use maven-ci-friendly versions plugin (and ci-friendly-flatten-maven-plugin)

* For each parent reactor of the project, the version should be defined in its `pom.xml` like this :

```xml
<project ...>

    <version>${revision}${sha1}${changelist}</version>

    <properties>

        <revision>1.0.3</revision>
        <changelist>-SNAPSHOT</changelist>
        <sha1></sha1>

    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>com.outbrain.swinfra</groupId>
                <artifactId>ci-friendly-flatten-maven-plugin</artifactId>
                <version>1.0.10</version>
                <executions>
                    <execution>
                        <goals>
                            <!-- Ensure proper cleanup. Will run on clean phase-->
                            <goal>clean</goal>
                            <!-- Enable ci-friendly version resolution. Will run on process-resources phase-->
                            <goal>flatten</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
```
> **Note:** Change the revison according to your needs

> **Note2:** If you use the check-license plugin, then the `.m2` directory should be add to the excluded. We have to put the .m2 at the same place than the project cause of the gitlab-ci caching system


* For each child module, the version of the parent should be defined in its `pom.xml` like this:

```xml
<project ...>

    <parent>

        <version>${revision}${sha1}${changelist}</version>

    </parent>

</project>
```

* For each pom dependencies referencing artifacts produced by another module of the project, the version of this dependency shoudl be define like this:

```xml

<project ...>

    <dependencies>
        <dependency>
            <groupId>org.opentestfactory</groupId>
            <artifactId>TFMessages</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>

</project>
```

When you have finished these configurations, the command below should work :

* `mvn clean package` should work and generate artifacts with a version corresponding to the `<revision>`, `<changelist>` and `<sha1>` define in your pom reactor

* `mvn -Drevision=<your_revison> -Dchangelist=<your_changelist> -Dsha1=<your_sha1> clean package` should work and generate artifacts with a version corresponding to the `<your_revision>`, `<your_changelist>` and `<your_sha1>` define in the command line

* `mvn ci-friendly-flatten:flatten` shoudl generate .ci-friendly-pom.xml where `<version>` should contain the compute version and not `${revision}${sha1}${changelist}` (`mvn ci-friendly-flatten:clean` to clean the generated file)


Ref :

* https://maven.apache.org/maven-ci-friendly.html
* https://github.com/outbrain/ci-friendly-flatten-maven-plugin

### Configuration for Sonarcloud

The configurations needed are the CI/CD variables and inside the `.gitlab-ci.yml`. No configuration is needed inside the `pom.xml`.

> **Note**: <span style="color:#e74e42">The following code is ***no longer*** to be used. The maven sonar plugin does it by default : </span>
```xml
<project ...>
    <properties>
        <sonar.projectKey>${project.groupId}:${project.artifactId}</sonar.projectKey>
        <sonar.projectName>${project.Name}</sonar.projectName>
    </properties>
</project>
```
> **Note 2**: Sonarqube is not longer to be used. It is replaced by [sonarcloud](https://sonarcloud.io/).

## Gitlab-ci variables

When you use this `gitlab-ci-java.yml` in your project, the CI exécution will need the variables below (in the project or from the parent groups):

* For maven repositories :
  * MAVEN_INTERNAL_REPO_USERNAME
  * MAVEN_INTERNAL_REPO_PASSWORD
  * MAVEN_MIRROR_INTERNAL_REPO_URL
  * MAVEN_RELEASE_INTERNAL_REPO_URL
  * MAVEN_SNAPSHOT_INTERNAL_REPO_URL

* Maven sttigs.xml
  * MAVEN_SETTINGS
    * Use the `settings.xml` file provided in this repository

* For Sonarcloud :
  * SONAR_HOST_URL
  * SONAR_TOKEN
  * SONARCLOUD_ORGANIZATION
